package com.qamaruddin.exhibitions;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import java.io.*;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.*;
import java.lang.Object;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author qamar-ud-din
 */
public class Utility {

    public String dirPath;

    public Integer scaleTWidth;

    public String sFileName;

    public String sSpreedSheet;

    public String csvPath;

    /**
     *
     */
    public Utility() {
        this.dirPath = "tmp";
        this.csvPath = "csv";
        this.scaleTWidth = 300;
        this.sFileName = "print.csv";
        this.sSpreedSheet = "output.xls";
    }

    /**
     *
     * @param _fileIn
     * @return
     */
    public String handleImage(File _fileIn) {
        try {
            BufferedImage img = ImageIO.read(_fileIn);
            int scaleX = (int) (this.scaleTWidth);
            int scaleY = (int) (img.getHeight() * this.scaleTWidth / img.getWidth());
            Image newImg = img.getScaledInstance(scaleX, scaleY, Image.SCALE_SMOOTH);
            BufferedImage buffered = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
            buffered.getGraphics().drawImage(newImg, 0, 0, null);
            long unixTime = System.currentTimeMillis() / 1000;
            String path = this.dirPath + File.separator + unixTime + ".jpg";
            if (ImageIO.write(buffered, "jpg", new File(path))) {
                return path;
            }
        } catch (IOException ex) {
            Logger.getLogger(Item.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param fileName
     * @return
     */
    public boolean appendCSV(String sRecord, String sCat) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(this.csvPath + File.separator + sCat + ".csv", true);
            writer.append(sRecord);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private String[] getCSVFiles() {
        File dir = new File(this.csvPath + File.separator);
        File[] files = dir.listFiles();
        String[] ls = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            ls[i] = files[i].getName();
        }
        return ls;
    }

    private byte[] bufferImageToBytes(String file) {
        ByteArrayOutputStream img_bytes = new ByteArrayOutputStream();
        try {
            BufferedImage is = ImageIO.read(new File(file));
            ImageIO.write(is, "jpg", img_bytes);
            img_bytes.flush();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return img_bytes.toByteArray();
    }

    public void createXLS() {
        HSSFCellStyle cellStyle = null;
        //Blank workbook
        HSSFWorkbook workbook = new HSSFWorkbook();
        cellStyle = workbook.createCellStyle();

        String[] lsFiles = this.getCSVFiles();
        for (int i = 0; i < lsFiles.length; i++) {
            this.createSheet(workbook, cellStyle, lsFiles[i]);
        }

        this.saveXLS(workbook);
    }

    private void createSheet(HSSFWorkbook workbook, HSSFCellStyle cellStyle, String sheetName) {
        BufferedReader br = null;
        String line = "";
        String csvseparator = ";;";
        //Create a blank sheet
        HSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultRowHeightInPoints((short) 70);
        sheet.setDefaultColumnWidth(20);
        try {
            br = new BufferedReader(new FileReader(this.csvPath + File.separator + sheetName));
            int rownum = 0;
            while ((line = br.readLine()) != null) {
                HSSFRow row = sheet.createRow(rownum);
                HSSFCell cell = null;
                String[] fields = line.split(csvseparator);
                for (int colnum = 0; colnum < fields.length; colnum++) {
                    cell = row.createCell(colnum);
                    cell.setCellStyle(cellStyle);
                    if (colnum == 0) {
                        this.insertPicture(workbook, sheet, fields[colnum], rownum, colnum);
                    } else {
                        cell.setCellValue(fields[colnum]);
                    }
                    sheet.autoSizeColumn(colnum);
                }
                rownum++;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void insertPicture(HSSFWorkbook workbook, HSSFSheet sheet, String filename, int rownum, int colnum) {
        int index = workbook.addPicture(this.bufferImageToBytes(filename), HSSFWorkbook.PICTURE_TYPE_JPEG);
        HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, (short) colnum, rownum, (short) (colnum + 1), rownum + 1);
        // other possible image types are: PICTURE_TYPE_PICT, PICTURE_TYPE_PNG,
        //                                 PICTURE_TYPE_WMF, PICTURE_TYPE_DIB, PICTURE_TYPE_EMF
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        HSSFPicture pic = patriarch.createPicture(anchor, index);
        // anchor type: ClientAnchor.MOVE_AND_RESIZE
        //              ClientAnchor.MOVE_DONT_RESIZE
        //              ClientAnchor.DONT_MOVE_AND_RESIZE
        anchor.setAnchorType(0);
    }

    private void saveXLS(HSSFWorkbook workbook) {
        try {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(this.sSpreedSheet));
            workbook.write(out);
            out.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public boolean deleteAll() {
        File tmp = null;
        try {
            String[] files = this.getCSVFiles();
            for (int i = 0; i < files.length; i++) {
                tmp = new File(this.csvPath + File.separator + files[i]);
                tmp.delete();
            }
            tmp = new File(this.sSpreedSheet);
            tmp.delete();
            return true;
        } catch (Exception ioe) {
            return false;
        }
    }
}
